FROM alpine:3.6 as builder

ENV V2RAY_VERSION 4.25.1

RUN apk --no-cache add --virtual .build-deps \
        curl \
    && curl -L https://github.com/v2ray/v2ray-core/releases/download/v$V2RAY_VERSION/v2ray-linux-64.zip >v2ray.zip \
    && mkdir -p /v2ray-src \
    && unzip v2ray.zip -d /v2ray-src \
    && mkdir -p /v2ray-dest/bin \
    && cd /v2ray-src \
    && cp v2ray v2ctl geoip.dat geosite.dat /v2ray-dest/bin \
    && chmod +x /v2ray-dest/bin/v2ray /v2ray-dest/bin/v2ctl \
    && mkdir -p /v2ray-dest/etc \
    && cp vpoint_vmess_freedom.json /v2ray-dest/etc/config.json


FROM scratch

COPY --from=builder /v2ray-dest/bin /bin

COPY --from=builder /v2ray-dest/etc /etc

ENTRYPOINT ["/bin/v2ray"]

CMD ["-config", "/etc/config.json"]
